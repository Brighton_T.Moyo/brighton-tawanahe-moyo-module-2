void main() {
  var app = App();

  app.app_name = "SnapScan";
  app.sector = "Consumer";
  app.developer = "Gerrit Greef";
  app.year_won = "2013";

  var app2 = App();
  app2.app_name = "SnapScan";

  print("Name of App : ${app.app_name}");
  print("Sector      : ${app.sector}");
  print("Developer   : ${app.developer}");
  print("Year Won    : ${app.year_won}");
  print("APP Name Capital letters : ${app2.app_name.toUpperCase()}");
}

class App {
  String app_name = " ";
  String sector = " ";
  String developer = " ";
  String year_won = " ";

  App() {}
}
